# read_spots --------------------------------------------------------------
test_that("read_spots imports spots data with: 1 worksheet x 1 product and 1 target audience", {
    file_path <- system.file('data-raw', 'csv', 'post_buy-sheet_3.csv',package = 'rianna', mustWork = TRUE)
    expect_s3_class(spot_data <- read_spots(file = file_path), "data.frame")
    expect_setequal(spot_data$Target, c("25-54"))
})

test_that("read_spots imports spots data with: 2 worksheets x (1 product and 1 target audience)", {
    file_path <- system.file('data-raw', 'xlsx', 'post_buy.xls',package = 'rianna', mustWork = TRUE)
    expect_s3_class(spot_data <- read_spots(file = file_path), "data.frame")
    expect_setequal(spot_data$Target, c("AP 18+", "25-54"))
})

test_that("read_spots imports spots data with: 1 worksheet x (1 product and 2 target audiences)", {
    file_path <- system.file('data-raw', 'xlsx', 'post_buy-multiple_audiences.xls',package = 'rianna', mustWork = TRUE)
    expect_s3_class(spot_data <- read_spots(file = file_path), "data.frame")
    expect_setequal(spot_data$Target, c("AP 18+", "25-54"))
})

test_that("read_spots imports spots data with: 1 worksheet x (2 product and 2 target audiences)", {
    file_path <- system.file('data-raw', 'xlsx', 'post_buy-multiple_products.xls',package = 'rianna', mustWork = TRUE)
    expect_s3_class(spot_data <- read_spots(file = file_path), "data.frame")
    expect_setequal(spot_data$Target, c("AP 18+", "25-54"))
    expect_setequal(spot_data$Product, c("Walkman", "Discman"))
})
