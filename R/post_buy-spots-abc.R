#' @title Read read_spots imports an Arianna post_by report on a Spots-level
#'
#' @param file (`character`) path to an Excel file with spot-level data.
#'
#' @return (`data.frame`) post by data table
#' @export
#'
#' @family Post Buy Reports
#' @examples
#' path <- system.file('data-raw', 'xlsx', 'post_buy.xls', package = 'rianna')
#' spot_data <- read_spots(path)
#' spot_data
read_spots <- function(file){
    switch (tolower(tools::file_ext(file)),
        csv = .spots$csv$read(file),
        xls = .spots$xlsx$read(file),
        stop("file type is not supported")
    )
}

.spots <- new.env()
